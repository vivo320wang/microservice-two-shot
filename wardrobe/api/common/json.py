from json import JSONEncoder
from django.urls import NoReverseMatch
from django.db.models import QuerySet
from datetime import datetime


class DateEncoder(JSONEncoder):
    def default(self, o):
        if isinstance(o, datetime):
            return o.isoformat()
        else:
            return super().default(o)


class QuerySetEncoder(JSONEncoder):
    def default(self, o):
        if isinstance(o, QuerySet):
            return list(o)
        else:
            return super().default(o)


class ModelEncoder(DateEncoder, QuerySetEncoder, JSONEncoder):
    encoders = {}

    def default(self, o):
        if isinstance(o, self.model):
            d = {}
            # if object, in this case, it's the Location mode, has the function get_api_url
            if hasattr(o, "get_api_url"):
                try:
                    # add the value to the dict of d = {} with key "href",
                    # this is why when hit the Create location endpoint in Insominia
                    # there is the "href" key and "api/locations/1" value on top of
                    # what is already in the LocationEnocder (id, closet_name, section_num and shelf_num)
                    
                    d["href"] = o.get_api_url() 
                except NoReverseMatch:
                    pass
            for property in self.properties:
                value = getattr(o, property)
                if property in self.encoders:
                    encoder = self.encoders[property]
                    value = encoder.default(value)
                d[property] = value
            d.update(self.get_extra_data(o))
            return d
        else:
            return super().default(o)

    def get_extra_data(self, o):
        return {}
