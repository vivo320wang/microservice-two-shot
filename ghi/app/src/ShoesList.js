function ShoesList(props) {
    console.log("Shoes HERE")
    if (props.shoes === undefined) {
    return null };

    return (
    <table className="table table-striped">
        
      <thead>
        <tr>
          <th>Manufacturer</th>
          <th>Model Name</th>
          <th>Color</th>
    
        </tr>
      </thead>
      <tbody>
      
        {   props.shoes.map((shoes) => {
            return (
            <tr key={ shoes.id } >  
              <td>{ shoes.manufacturer }</td>
              <td>{ shoes.model_name }</td>
              <td>{ shoes.color } </td>
              <td>{ shoes.picture_url}</td>
              <td>{ shoes.bin }</td>
            </tr>
        )}
          )}
        
      </tbody>
    </table>
  );
}

export default ShoesList;

    