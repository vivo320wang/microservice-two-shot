import React from 'react';

class ShoesForm extends React.Component {
    constructor(props) {
    super(props);
    this.state = {
        manufacturer: '',
        model_name: '',
        color: '',
        bin: '',
    };
    this.handleChangeManufacturer = this.handleChangeManufacturer.bind(this);
    this.handleChangeModelName = this.handleChangeModelName.bind(this);
    this.handleChangeColor = this.handleChangeColor.bind(this);
    this.handleChangeBin = this.handleChangeBin.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    }
    
async componentDidMount() {
    const url = 'http://localhost:8100/api/bins/';
    
    const response = await fetch(url);
    
    if (response.ok) {
        const data = await response.json();
        this.setState({ bins: data.bins });
        }
    }

async handleSubmit(event) {
  event.preventDefault();
  const data = {...this.state};
  delete data.shoes;
  
  const locationUrl = 'http://localhost:8080/api/shoes/';
  const fetchConfig = {
    method: "post",
    body: JSON.stringify(data),
    headers: {
    'Content-Type': 'application/json',
    },
};
  const response = await fetch(locationUrl, fetchConfig);
  if (response.ok) {
    const newShoes = await response.json();
    console.log(newShoes);
    this.setState({
        manufacturer: '',
        model_name: '',
        color: '',
        bin: '',

    });
    }
}

handleChangeManufacturer(event) {
  const value = event.target.value;
  this.setState({ manufacturer: value});
}

handleChangeModelName(event) {
    const value = event.target.value;
    this.setState({ model_name: value});
}

handleChangeColor(event) {
    const value = event.target.value;
    this.setState({ color: value});
}

handleChangeBin(event) {
    const value = event.target.value;
    this.setState({ bin: value});
}


render(props) {
    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create new shoes</h1>
            <form onSubmit={this.handleSubmit} id="create-conference-form">
              <div className="form-floating mb-3">
                <input onChange={this.handleManufacturerName} placeholder="Manufacturer" required type="text" name="manufacturer" id="manufacturer" className="form-control" />
                <label htmlFor="manufacturer">Manufacturer</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleChangeModelName} placeholder="Model Name" required type="text" name="model_name" id="model_name" className="form-control" />
                <label htmlFor="model_name">Model Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleChangeColor} placeholder="Color" required type="text" name="color" id="color" className="form-control" />
                <label htmlFor="color">Color</label>
              </div>
              {/* <div className="mb-3">
                <select onChange={this.handleChangeBin} required name="bin" id="bin" className="form-select">
                  <option value="">Choose a bin</option>
                  {this.state.bins.map(bin => {
                    return (
                      <option key={bin.id} value={bin.id}>{bin.bin_number}</option>
                    )
                  })}
                </select>
              </div> */}
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}



export default ShoesForm;