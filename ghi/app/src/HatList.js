function HatList(props){
    if(props.hats === undefined){
    return null;
  }
    return (
    <table className="table table-striped">
      <thead>
        <tr>
          <th>Style</th>
          <th>Color</th>
          <th>Fabric</th>
          <th>Location</th>
        </tr>
      </thead>
      <tbody>
        {props.hats.map(hat=> {
          return (
          <tr key={hat.id}>
            <td>{ hat.style_name }</td>
            <td>{ hat.color }</td>
            <td>{ hat.fabric }</td>
            <td>{ hat.location.closet_name }</td>
          </tr>
  );
})}
</tbody>
    </table>

);}

export default HatList;