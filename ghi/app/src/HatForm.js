import React from "react";

class HatForm extends React.Component {
    constructor(props){
        super(props)
        this.state = {hats: []};
        this.handleStyleChange = this.handleStyleChange.bind(this);
        this.handleColorChange = this.handleColorChange.bind(this);
        this.handleFabricChange = this.handleFabricChange.bind(this);
        this.handleLocationChange = this.handleLocationChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    
    handleStyleChange(event){
        const value = event.target.value;
        this.setState({style:value})
    }   

    handleColorChange(event){
        const value = event.target.value;
        this.setState({color:value})
    }   

    handleFabricChange(event){
        const value = event.target.value;
        this.setState({fabric:value})
    }   

    handleLocationChange(event){
        const value = event.target.value;
        this.setState({location:value})
    }   

    handleSubmit(event){
        event.preventDefault();
        const data = {...this.state};
        delete data.hats;
        console.log(data);




    }
    async componentDidMount(){
        const url = "http://localhost:8090/hats/";
        
        const response = await fetch(url)

        if (response.ok){
            const data = await response.json();
            
            this.setState({hats: data.hats})

            // const selectTag = document.getElementById('location');
            // for (let hat of data.hats){
            //     const option = document.createElement('option');
            //     option.value = hat.location;
            //     option.innerHTML = hat.location.closet_name;
            //     selectTag.appendChild(option)
            // }
        }
        
    }
    render() {
      return (
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1>Create a new hat</h1>
              <form id="create-hat-form">
                <div className="form-floating mb-3">
                  <input onChange ={this.handleStyleChange} placeholder="Style" required type="text" name="style" id="style" className="form-control" />
                  <label htmlFor="style">Style</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange ={this.handleColorChange} placeholder="Color" required type="text" name="color" id="color" className="form-control" />
                  <label htmlFor="color">Color</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange ={this.handleFabricChange} placeholder="Fabric" required type="text" name="fabric" id="fabric" className="form-control" />
                  <label htmlFor="fabric">Fabric</label>
                </div>
                <div className="mb-3">
                  <select onChange ={this.handleLocationChange} required name="location" id="location" className="form-select">
                    <option value="">Choose a location</option>
                    {this.state.hats.map(hat => {
                        return (
                            <option key= {hat.id} value = {hat.id}>
                                {hat.location.closet_name}
                            </option>
                        )
                    })}
                  </select>
                </div>
                <button className="btn btn-primary">Create</button>
              </form>
            </div>
          </div>
        </div>
      );
    }
  }
export default HatForm