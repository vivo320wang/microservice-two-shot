import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import HatForm from './HatForm';
import HatList from './HatList';
import ShoesList from './ShoesList';
import ShoesForm from './ShoesForm';



function App(props) {
  return (
    <BrowserRouter>
      <Nav />
      <HatList />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
        <Route path="shoes">
          <Route path="/shoes/" element={ <ShoesList shoes={props.shoes}/>} />
        </Route>
        <Route path="shoes">
            <Route path="new" element={<ShoesForm shoes={props.shoes} />} />
          </Route>
        </Routes>
      </div>
      <HatForm />
      {/* <HatList hats={props.hats} /> */}
    </BrowserRouter>
  );
}



export default App;
