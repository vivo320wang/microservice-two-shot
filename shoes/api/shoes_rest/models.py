from django.db import models

# Create your models here.

class ShoeBinVO(models.Model):
    closet_name = models.CharField(max_length=250)
    bin_number = models.PositiveSmallIntegerField()
    bin_size = models.PositiveSmallIntegerField()
    import_href = models.CharField(max_length=200, unique=True)




class Shoes(models.Model):
    manufacturer = models.CharField(max_length=250)
    model_name = models.CharField(max_length=150)
    color = models.CharField(max_length=100)
    picture_url = models.URLField(null=True)
    bin = models.ForeignKey(
        ShoeBinVO, related_name="bin",
        on_delete=models.CASCADE, null=True,
    )
    def __str__(self):
        return self.name 


