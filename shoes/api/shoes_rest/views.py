from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from common.json import ModelEncoder
from .models import Shoes, ShoeBinVO
import json

class ShoesListEncoder(ModelEncoder):
    model = Shoes 
    properties = [
    "manufacturer",
    "model_name",
    "color", 
    "picture_url",
    "bin",
    ]


@require_http_methods(["GET", "POST"])
def api_list_shoes(request):

    if request.method == "GET":
        shoes = Shoes.objects.all()
        return JsonResponse(
            {"shoes": shoes},
            encoder=ShoesListEncoder,
        )
    else:
        content = json.loads(request.body)
    try:
        bin = ShoeBinVO.objects.get(id=content["bin"])
        content["bin"] = bin
    except ShoeBinVO.DoesNotExist:
        return JsonResponse(
            {"message": "Invalid bin id"},
            status=400,
        )

    shoes = Shoes.objects.create(**content)
    return JsonResponse(
        shoes,
        encoder=ShoesListEncoder,
        safe=False,
    )


            
@require_http_methods(["DELETE", "GET", "PUT"])
def api_shoes(request, pk):

    if request.method == "GET":
        shoes = Shoes.objects.get(id=pk)
        return JsonResponse(
            shoes,
            encoder=ShoeBinVO
        )
        try:
            shoes = Shoes.objects.get(id=pk)
            return JsonResponse(
                shoes,
                encoder=ShoesListEncoder,
                safe=False
            )
        except Shoes.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            shoes = Shoes.objects.get(id=pk)
            shoes.delete()
            return JsonResponse(
                shoes, 
                encoder=ShoesListEncoder,
                safe=False,
            )
        except Shoes.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
    else:
        try:
            props = ["closet_name", "bin_number", "bin_size"]
            for prop in props:
                if prop in content:
                    setattr(bin, prop, content[prop])
            bin.save()
            return JsonResponse(
                bin,
                encoder=BinEncoder,
                safe=False,
            )
        except Bin.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
