# Generated by Django 4.0.3 on 2022-06-16 03:50

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('hats_rest', '0003_hats_location'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='locationvo',
            options={'ordering': ('closet_name', 'section_number', 'shelf_number')},
        ),
    ]
