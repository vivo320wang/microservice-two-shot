from django.shortcuts import render
from .models import Hats, LocationVO
from common.json import ModelEncoder
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
import json

# Create your views here.


class LocationVOEncoder(ModelEncoder):
    model = LocationVO
    properties = [
        "import_href",
        "closet_name",
        "section_number",
        "shelf_number",
    ]


class HatDetailEncoder(ModelEncoder):
    model = Hats
    properties = ['id', 'fabric', "style_name", "color", "picture_url", "location", ]
    encoders = {'location': LocationVOEncoder()}

# need LocationVOEncoder because location is a foreignkey,
# Json doesnt know what to do with this, so need to encode 

@require_http_methods(["GET", "POST"])
def api_list_hats(request):
    if request.method == "GET":
        hats = Hats.objects.all()
        return JsonResponse(
                {'hats': hats},
                encoder=HatDetailEncoder,
            )

    else:
        content = json.loads(request.body)

        try:
            location_id = content['location']
            location = LocationVO.objects.get(import_href=location_id)
            content["location"] = location

        except LocationVO.DoesNotExist:
            return JsonResponse(
                {'message': "Invalid location id"},
                status=400,
            )

        hat = Hats.objects.create(**content)
        return JsonResponse(
            hat,
            encoder=HatDetailEncoder,
            safe=False,
            )


@require_http_methods(["GET", "DELETE"])
def api_delete_hat(request, pk):
    if request.method == "GET":
        try:
            hat = Hats.objects.get(id=pk)
            return JsonResponse(
                hat,
                encoder=HatDetailEncoder,
                safe=False,
            )
        except Hats.DoesNotExist:
            return JsonResponse(
                    {'message': 'Does not exist'},
                    status=400,
                )

    elif request.method == "DELETE":
        try:
            hat = Hats.objects.get(id=pk)
            hat.delete()
            return JsonResponse(
                hat,
                encoder=HatDetailEncoder,
                safe=False,
            )
        except Hats.DoesNotExist:
            return JsonResponse(
                {'message': 'Does not exist'},
                status=400,
            )
